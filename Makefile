all: generate

.PHONY: generate
generate:
	rm -rf ./out/dump.md
	find . -name "Lesson.md" -exec cat '{}' >> out/dump.md \;

.PHONY: prepare
prepare:
	mkdir out/

.PHONY: clean
clean: out/
	rm -rf out/