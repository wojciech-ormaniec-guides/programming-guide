# Intro

## What is CMake?

You can't be more precise than, the [home page](https://cmake.org/) of a project :

> CMake is an open-source, cross-platform family of tools designed to build, test and package software. CMake is used to control the software compilation process using simple platform and compiler independent configuration files, and generate native makefiles and workspaces that can be used in the compiler environment of your choice. The suite of CMake tools were created by Kitware in response to the need for a powerful, cross-platform build environment for open-source projects such as ITK and VTK.

To put it in simple words, `CMake` is a tool which makes building your `c++`/`c` project easy.

## What can CMake do for me?

To be fully honest — anything you want it to. Do you want simple way to compile your project? Do you want to run scripts in certain situations? Do you want to link latest versions of frameworks or libraries to your project? CMake does it. 

## Why do I need this course?

To be honest, you do not. I learned CMake on my own, with just a documentation and [stackoverflow](https://stackoverflow.com), which is more than enough. Only thing I can promise you, is that I will not drop any bombs on you, and that I will quickly go over almost every functionality of CMake with just a couple new lines of code per lesson. I will show you documentation page of currently used version, and explain almost every nook and cranny of this tool. 